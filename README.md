
# Wormball

Eat colorpoints to grow, be the last and the longest! Beat the Highscore!

![](screenshot.png)

Contributors: MisterE



Wormball mod by MisterE for use with arena_lib (minigame_lib)

Code snippets from https://gitlab.com/zughy-friends-minetest/arena_lib/-/blob/master/DOCS.md and from Zughy's minigames
all assets cc0 except music (see end of file). Models by MisterE, textures by MisterE, and sounds CC0 from opengameart

to set up an arena, create a completely enclosed arena out of any nodes you wish except those provided by this mod. 
You should place lights in the walls, or have glass to allow light in.
You may decorate the walls how you wish. 
Keep in mind that very large arenas may cause server lag during loading, and they are in general not fun to play in anyways.
Make the arena, then follow the instructions below:

Basic setup:

1) Make your arena. It should be a completely enclosed area, but can be any shape. Protect the arena.

2) /wormball create <arena_name> -- creates a new arena instance

3) /wormball edit <arena_name> -- enters the editor

4) use the editor to place a minigame sign (outside the arena, where players can get to it (lobby? Spawn? up 2 u), 
	assign it to your minigame using the sign with the plus tool. 

5) using the editor tools, inside the arena, mark player spawner locations. 

6) using the special wormball editor section in the arena editor menu, set position 1 and 2 for the arena. 
	These points mark the area that the game will clear of game pieces between games and where food will spawn. 
	Make sure that the two points completely envelop your arena area, but are no larger than your arena area.
	when you use the pos 1 and pos 2 tools, the respective position is set to your location (the location of your feet). 
	Use noclip as necassary to place these locations. 

7) If you wish (strongly suggested) use the bgm option in the minigame editor to set background music. The two titles provided by this mod are listed below.

7) exit the editor mode

8) type /minigamesettings wormball

9) change the hub spawnpoint to be next to the signs.




==================================================================
the formula used to determine whether to start erasing powerup dots is: 
if # dots in existence > min_food_factor * #players + min_food then
--have a 1/2 chance to erase a dot
end
==================================================================






background music is available: it is in the sounds folder: 

***note all music was changed by converting it to .ogg***

track filename .ogg	| Original track name		| Author		| License							| Link
==========================================================================================================================================================================
wormball_bgm		|  My Arcade.wav		| Gamer73		| CC0	https://creativecommons.org/publicdomain/zero/1.0/	| https://freesound.org/people/Gamer73/sounds/455016/
==========================================================================================================================================================================
chiptune_one		| Chiptune One.wav		| CarlosCarty		| CC BY 3.0  https://creativecommons.org/licenses/by/3.0/	| https://freesound.org/people/CarlosCarty/sounds/427513/
==========================================================================================================================================================================













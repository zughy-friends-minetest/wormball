local storage = wormball.storage
local S = core.get_translator("wormball")

local function send_message(arena,num_str)
    arena_lib.HUD_send_msg_all("title", arena, S("Game Begins In").. " " ..num_str, 1,nil,0xFF0000)
    -- ref: arena_lib.HUD_send_msg_all(HUD_type, arena, msg, <duration>, <sound>, <color>)
end

arena_lib.on_load("wormball", function(arena)

    -- attach players to an entity to prevent them from moving
    core.after(.1,function( arena )

        -- attach the players so they cannot move until the time starts. Add nametags.
        for pl_name, stats in pairs(arena.players) do 
            local player = core.get_player_by_name(pl_name)
            if player then 
                local pl_pos = player:get_pos()
                local keeper = core.add_entity(pl_pos,"gems_battle:player_keeper")
                player:set_attach(keeper)
            end
        end

    end,arena)

    -- load the saved highscore data from disk

    local wormball_highscores = storage:get_string(arena.name .. "_highscores")


    local leaderboard = {}


    if wormball_highscores then leaderboard = core.deserialize( wormball_highscores ) end

    
    arena.singleplayer_leaderboard = leaderboard
    arena.multi_scores = {}
    
    

    --HUD countdown
    send_message(arena,'5')
    core.after(1, function(arena)
        send_message(arena,'4')
        core.after(1, function(arena)
            send_message(arena,'3')
            core.after(1, function(arena)
                send_message(arena,'2')
                core.after(1, function(arena)
                    send_message(arena,'1')
                    core.after(1, function(arena)
                        arena_lib.HUD_send_msg_all("title", arena, S("GO!"), 1,nil,0x00FF00)
                        core.after(1, function(arena)
                            arena_lib.HUD_send_msg_all("hotbar", arena, S("Avoid Your Own Color, eat other dots!"), 5,nil,0xFFAE00)
        
                        end, arena)
                    end, arena)

                end, arena)
                
            end, arena)
    
        end, arena)
    
    end, arena)


    local idx = 1

    for pl_name, stats in pairs(arena.players) do

        --send control messages

        local message = S("Controls: Use look direction to steer, or press jump or sneak to move.  Dont bump anything!")
        core.chat_send_player(pl_name,message)
        local message = S("Eat dots to grow and get points, but your own color will shrink you!")
        core.chat_send_player(pl_name,message)
 
        local player = core.get_player_by_name(pl_name)
        local pos = player:get_pos()

        
        -- assign colors to players
        arena.players[pl_name].color = wormball.color_names[idx]


        
        --save players' textures so they can be made invisible
        wormball.player_texture_save[pl_name] = player:get_properties().textures
        --set their textures to invisible
        player:set_properties({textures = {'wormball_alpha.png'}})
        --set their first node (head) position
        arena.players[pl_name].nodes = {pos}
        local look_dir = wormball.get_look_dir(arena,player)
        wormball.place_node(  arena.players[pl_name].nodes  ,  {x=0,y=1,z=0}  ,  {x=0,y=1,z=0}  ,  look_dir,   arena.players[pl_name].color  )

        --determine whether it is singleplayer or multiplayer (singleplayer is set as default in arena temp props)
        if idx > 1 then
            arena.mode = 'multiplayer'
        end
        idx = idx + 1

        
    end

    --disable spectate for singleplayer, allow it for multiplayer. Spectate is buggy with singleplayer... but works with multi now.
    if arena.mode == 'singleplayer' then
        arena.spectate_mode = false
    else
        arena.spectate_mode = true
    end

end)





core.register_entity("wormball:player_keeper",{
    initial_properties = {
        visual = "sprite",
        physical = false,
        hp_max = 32,
        textures = {"blank.png"},
        collisionbox = {-.1,-.1,-.1,.1,.1,.1},
    },
    
    _timer = 0,
    on_step = function(self,dtime)

        self._timer = self._timer + dtime
        if self._timer >= 10 then
            local children = self.object:get_children()
            if #children >= 1 then 
                children[1]:set_detach()
            end
            self.object:remove()
        end
    end,
})